//
//  CoreDataUtility.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

#import "CoreDataUtility.h"
#import "Transaction.h"
#import "Configuration.h"
#import "Build.h"
#import "TPR.h"

static CoreDataUtility *sharedInstance = nil;

@implementation CoreDataUtility

#pragma mark
#pragma mark ---- data base operations ----
#pragma mark ---- Fetching EOS ----

- (NSArray*)fetchEntityNamed:(NSString*)entityName withEditContext:(NSManagedObjectContext*)ctx predicate:(NSPredicate*)q sortDescriptors:(NSArray*)sortOrders
{
    NSError *error = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:ctx];
    
    [request setEntity:entity];
    
    if (q) {
        [request setPredicate:q];
    }
    
    if (sortOrders) {
        [request setSortDescriptors:sortOrders];
    }
    
    return [ctx executeFetchRequest:request error:&error];
    
}

- (NSArray*)fetchEntityNamed:(NSString*)entityName withEditContext:(NSManagedObjectContext*)ctx predicate:(NSPredicate*)q
{
    return [self fetchEntityNamed:entityName withEditContext:ctx predicate:q sortDescriptors:nil];
}

- (NSArray*)fetchAllBuildsWithEditContext:(NSManagedObjectContext*)ctx
{
    return [self fetchEntityNamed:@"Build" withEditContext:ctx
                        predicate:nil
                  sortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO]]];
}

- (NSArray*)fetchAllTransactionsWithEditContext:(NSManagedObjectContext*)ctx
{
    return [self fetchEntityNamed:@"Transaction"
                  withEditContext:ctx
                        predicate:nil
                  sortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO]]];
}

- (Transaction*)fetchTransactionMatchingCreateDate:(NSDate*)createDate withEditContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *q = nil;
    
    q = [self equalToPredicateForKey:@"createDate" andValue:createDate];
    return [[self fetchEntityNamed:@"Transaction" withEditContext:ctx predicate:q] lastObject];
}

- (Configuration*)fetchConfigurationWithEditContext:(NSManagedObjectContext*)ctx
{
    return [[self fetchEntityNamed:@"Configuration" withEditContext:ctx predicate:nil] lastObject];
}

- (Build*)fetchBuildMatchingIdentifier:(NSString*)identifier createdBetweenDates:(NSDictionary*)dateMapping withEditContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *q;
    NSPredicate *lq;
    NSPredicate *rq;
    NSDate *lowerBoundDate;
    NSDate *upperBoundDate;
    
    lowerBoundDate = dateMapping[@"lower"];
    upperBoundDate = dateMapping[@"upper"];
    q = [self equalToPredicateForKey:@"identifier" andValue:identifier];
    lq = [self greaterThanOrEqualToPredicateForKey:@"createDate" andValue:lowerBoundDate];
    rq = [self lessThanPredicateForKey:@"createDate" andValue:upperBoundDate];
    lq = [NSCompoundPredicate andPredicateWithSubpredicates:@[lq,rq]];
    q = [NSCompoundPredicate andPredicateWithSubpredicates:@[q,lq]];
    return [[self fetchEntityNamed:@"Build" withEditContext:ctx predicate:q] lastObject];
}

- (TPR*)fetchTPRMatchingZipFilename:(NSString*)name build:(Build*)build withEditContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *q;
    NSPredicate *lq;
    
    q = [self equalToPredicateForKey:@"zipFilename" andValue:name];
    lq = [self equalToPredicateForKey:@"build" andValue:build];
    q = [NSCompoundPredicate andPredicateWithSubpredicates:@[q,lq]];
    return [[self fetchEntityNamed:@"TPR" withEditContext:ctx predicate:q] lastObject];
}

#pragma mark ---- New EOS ----

- (id)insertNewEONamed:(NSString*)entityName editContext:(NSManagedObjectContext*)ctx
{
	id newEO = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:ctx];
	return newEO;
}

- (Transaction*)insertNewTransactionWithEditContext:(NSManagedObjectContext*)ctx
{
	return [self insertNewEONamed:@"Transaction" editContext:ctx];
}

- (Configuration*)insertNewConfigurationWithEditContext:(NSManagedObjectContext*)ctx
{
    return [self insertNewEONamed:@"Configuration" editContext:ctx];
}

- (Build*)insertNewBuildWithEditContext:(NSManagedObjectContext*)ctx
{
    return [self insertNewEONamed:@"Build" editContext:ctx];
}

- (TPR*)insertNewTPRWithEditContext:(NSManagedObjectContext*)ctx
{
    return [self insertNewEONamed:@"TPR" editContext:ctx];
}

#pragma mark
#pragma mark ---- Pre-fabbed predicates ----

- (NSPredicate*)equalToPredicateForKey:(NSString*)key andValue:(id)value caseInsensitive:(BOOL)yesNo
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = nil;
	
	if (yesNo) {
		q = [NSComparisonPredicate
			 predicateWithLeftExpression:lhs
			 rightExpression:rhs
			 modifier:NSDirectPredicateModifier
			 type:NSEqualToPredicateOperatorType
			 options:NSCaseInsensitivePredicateOption];
	}else{
		q = [NSComparisonPredicate
			 predicateWithLeftExpression:lhs
			 rightExpression:rhs
			 modifier:NSDirectPredicateModifier
			 type:NSEqualToPredicateOperatorType
			 options:0];
	}
	
	return q;
}

- (NSPredicate*)equalToPredicateForKey:(NSString*)key andValue:(id)value
{
	return [self equalToPredicateForKey:key andValue:value caseInsensitive:NO];
}

- (NSPredicate*)equalToAnyPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSAnyPredicateModifier
					  type:NSEqualToPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)equalToAllPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSAllPredicateModifier
					  type:NSEqualToPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)notEqualToPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSNotEqualToPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)greaterThanPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSGreaterThanPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)greaterThanOrEqualToPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSGreaterThanOrEqualToPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)lessThanPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSLessThanPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)lessThanOrEqualToPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSLessThanOrEqualToPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)beginsWithPredicateForKey:(NSString*)key andValue:(id)value caseInsensitive:(BOOL)yesNo
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = nil;
    
    if (yesNo) {
        q = [NSComparisonPredicate
             predicateWithLeftExpression:lhs
             rightExpression:rhs
             modifier:NSDirectPredicateModifier
             type:NSBeginsWithPredicateOperatorType
             options:NSCaseInsensitivePredicateOption];
    }else{
        q = [NSComparisonPredicate
             predicateWithLeftExpression:lhs
             rightExpression:rhs
             modifier:NSDirectPredicateModifier
             type:NSBeginsWithPredicateOperatorType
             options:0];
    }
	return q;
}

- (NSPredicate*)beginsWithPredicateForKey:(NSString*)key andValue:(id)value
{
	return [self beginsWithPredicateForKey:key andValue:value caseInsensitive:NO];
}

- (NSPredicate*)endsWithPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSEndsWithPredicateOperatorType
					  options:0];
	return q;
}

- (NSPredicate*)likeWithPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSLikePredicateOperatorType
					  options:NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption];
    
	return q;
}

- (NSPredicate*)matchesWithPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSDirectPredicateModifier
					  type:NSMatchesPredicateOperatorType
					  options:NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption];
    
	return q;
}

/**
 A predicate to match with any entry in the destination of a to-many relationship.
 The left hand side must be a collection. The corresponding predicate compares each
 value in the left hand side against the right hand side and returns YES when it
 finds the first match—or NO if no match is found
 **/
- (NSPredicate*)likeAnyWithPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSAnyPredicateModifier
					  type:NSMatchesPredicateOperatorType
					  options:NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption];
	return q;
}

/**
 A predicate to compare all entries in the destination of a to-many relationship.
 The left hand side must be a collection. The corresponding predicate compares each
 value in the left hand side with the right hand side, and returns NO when it
 finds the first mismatch—or YES if all match.
 **/
- (NSPredicate*)likeAllWithPredicateForKey:(NSString*)key andValue:(id)value
{
	NSExpression *lhs = [NSExpression expressionForKeyPath:key];
	NSExpression *rhs = [NSExpression expressionForConstantValue:value];
	NSPredicate *q = [NSComparisonPredicate
					  predicateWithLeftExpression:lhs
					  rightExpression:rhs
					  modifier:NSAllPredicateModifier
					  type:NSLikePredicateOperatorType
					  options:NSCaseInsensitivePredicateOption];
	return q;
}

- (NSPredicate*)inWithPredicateForKey:(NSString*)key andValues:(NSArray*)values
{
	return [NSPredicate predicateWithFormat: @"(%K IN %@)", key, values];
}

- (NSPredicate*)inAnyWithPredicateForKey:(NSString*)key andValues:(NSArray*)values
{
	return [NSPredicate predicateWithFormat: @"(ANY %K IN %@)", key, values];
}

- (NSPredicate*)notInWithPredicateForKey:(NSString*)key andValues:(NSArray*)values
{
	NSPredicate *subQ = nil;
	
	subQ = [self inWithPredicateForKey:key andValues:values];
	subQ = [NSCompoundPredicate notPredicateWithSubpredicate:subQ];
	
	return subQ;
}

#pragma mark ---- singleton object methods ----

- (id) init {
	self = [super init];
	if (self != nil) {
        
	}
	return self;
}

// See "Creating a Singleton Instance" in the Cocoa Fundamentals Guide for more info
+ (CoreDataUtility*)sharedInstance
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            id localSelf;
            
            localSelf = [[self alloc] init]; // assignment not done here
        }
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone*)zone {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;  // assignment and return on first allocation
        }
    }
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone*)zone
{
    return self;
}

@end
