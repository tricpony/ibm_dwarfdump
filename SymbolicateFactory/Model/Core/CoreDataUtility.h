//
//  CoreDataUtility.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConstantsDefines.h"

@class Transaction;
@class Configuration;
@class Build;
@class TPR;

@interface CoreDataUtility : NSObject

+ (CoreDataUtility*)sharedInstance;


/**
 Inserting
 **/
- (Transaction*)insertNewTransactionWithEditContext:(NSManagedObjectContext*)ctx;
- (Configuration*)insertNewConfigurationWithEditContext:(NSManagedObjectContext*)ctx;
- (Build*)insertNewBuildWithEditContext:(NSManagedObjectContext*)ctx;
- (TPR*)insertNewTPRWithEditContext:(NSManagedObjectContext*)ctx;

- (NSArray*)fetchAllBuildsWithEditContext:(NSManagedObjectContext*)ctx;
- (NSArray*)fetchAllTransactionsWithEditContext:(NSManagedObjectContext*)ctx;
- (Transaction*)fetchTransactionMatchingCreateDate:(NSDate*)createDate withEditContext:(NSManagedObjectContext*)ctx;

- (Configuration*)fetchConfigurationWithEditContext:(NSManagedObjectContext*)ctx;
- (Build*)fetchBuildMatchingIdentifier:(NSString*)identifier createdBetweenDates:(NSDictionary*)dateMapping withEditContext:(NSManagedObjectContext*)ctx;
- (TPR*)fetchTPRMatchingZipFilename:(NSString*)name build:(Build*)build withEditContext:(NSManagedObjectContext*)ctx;

/**
 Pre-fabbed predicates
 **/
- (NSPredicate*)equalToPredicateForKey:(NSString*)key andValue:(id)value caseInsensitive:(BOOL)yesNo;
- (NSPredicate*)equalToAnyPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)equalToAllPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)equalToPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)notEqualToPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)greaterThanPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)greaterThanOrEqualToPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)lessThanPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)lessThanOrEqualToPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)beginsWithPredicateForKey:(NSString*)key andValue:(id)value caseInsensitive:(BOOL)yesNo;
- (NSPredicate*)beginsWithPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)endsWithPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)likeWithPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)matchesWithPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)likeAnyWithPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)likeAllWithPredicateForKey:(NSString*)key andValue:(id)value;
- (NSPredicate*)inWithPredicateForKey:(NSString*)key andValues:(NSArray*)values;
- (NSPredicate*)inAnyWithPredicateForKey:(NSString*)key andValues:(NSArray*)values;
- (NSPredicate*)notInWithPredicateForKey:(NSString*)key andValues:(NSArray*)values;

@end
