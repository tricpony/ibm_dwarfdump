//
//  Transaction.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum
{
    kExecutedPass = 0,
    kExecutedFail,
    kNeverExecuted
}  FactoryExecutionStatus;

@class TPR;

NS_ASSUME_NONNULL_BEGIN

@interface Transaction : NSManagedObject
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *buildIdentifier;
@property (nonatomic, readonly) NSString *tprPath;
@property (nonatomic, readonly) NSString *buildPath;
@property (nonatomic, readonly) NSString *tprZipFilenameWithExtension;
@property (nonatomic, readonly) NSString *tprZipFilename;
@property (nonatomic, readonly) FactoryExecutionStatus executionStatusValue;
@property (nonatomic, readonly) NSString *executionStatusString;

+ (NSArray*)groupedTransactionsByCreateDateWithEditContext:(NSManagedObjectContext*)ctx;

@end

NS_ASSUME_NONNULL_END

#import "Transaction+CoreDataProperties.h"
