//
//  Configuration.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/27/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Configuration : NSManagedObject

+ (Configuration*)myConfigurationWithEditContext:(NSManagedObjectContext*)ctx;

@end

NS_ASSUME_NONNULL_END

#import "Configuration+CoreDataProperties.h"
