//
//  Build.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TPR;

NS_ASSUME_NONNULL_BEGIN

@interface Build : NSManagedObject

+ (Build*)buildForIdentifier:(NSString*)identifier withEditContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)groupedBuildsByCreateDateWithEditContext:(NSManagedObjectContext*)ctx;

@end

NS_ASSUME_NONNULL_END

#import "Build+CoreDataProperties.h"
