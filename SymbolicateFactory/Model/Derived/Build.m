//
//  Build.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "Build.h"
#import "TPR.h"
#import "CoreDataUtility.h"
#import "NSDate+Factory.h"

@implementation Build

/**
 Returns the build matching identifier that was created today, otherwise return a new build
 **/
+ (Build*)buildForIdentifier:(NSString*)identifier withEditContext:(NSManagedObjectContext*)ctx
{
    Build *nextBuild;
    NSDate *midnightToday;
    NSDate *midnightTomorrow;
    NSDate *today = [NSDate date];
    NSDictionary *mapping;
    
    midnightToday = [today midnight];
    midnightTomorrow = [[today midnight] dateByAddingDays:1];
    mapping = @{@"lower":midnightToday,@"upper":midnightTomorrow};
    nextBuild = [[CoreDataUtility sharedInstance] fetchBuildMatchingIdentifier:identifier createdBetweenDates:mapping withEditContext:ctx];
    if (!nextBuild) {
        nextBuild = [[CoreDataUtility sharedInstance] insertNewBuildWithEditContext:ctx];
        nextBuild.identifier = identifier;
    }
    
    return nextBuild;
}

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    //all eos have a create date so set it here
    [self performSelector:@selector(setCreateDate:) withObject:[NSDate date]];
}

/**
 Returns an ordered collection of grouped builds
 **/
+ (NSArray*)groupedBuildsByCreateDateWithEditContext:(NSManagedObjectContext*)ctx
{
    NSArray *allBuilds;
    Build *firstBuild;
    NSMutableArray *groups = nil;
    NSMutableDictionary *group = nil;
    NSMutableArray *groupBuilds = nil;
    NSString *datePattern = @"MMM d yy";
    
    //these have already been sorted in descending order which means the most recent transaction will be first and oldest last
    allBuilds = [[CoreDataUtility sharedInstance] fetchAllBuildsWithEditContext:ctx];
    firstBuild = [allBuilds firstObject];
    groups = [NSMutableArray arrayWithCapacity:1];
    
    for (Build *build in allBuilds) {
        
        if (!groupBuilds) {
            
            group = [NSMutableDictionary dictionaryWithCapacity:5];
            [groups addObject:group];
            groupBuilds = [NSMutableArray arrayWithCapacity:5];
            group[@"date_label"] = [build.createDate formattedDateForPattern:datePattern];
            group[@"builds"] = groupBuilds;
            [groupBuilds addObject:build];
            firstBuild = build;
            
        }else if ([firstBuild.createDate isSameDayOfYear:build.createDate]) {
            
            [groupBuilds addObject:build];
            
        }else{
            group = [NSMutableDictionary dictionaryWithCapacity:5];
            [groups addObject:group];
            groupBuilds = [NSMutableArray arrayWithCapacity:5];
            group[@"date_label"] = [build.createDate formattedDateForPattern:datePattern];
            group[@"builds"] = groupBuilds;
            [groupBuilds addObject:build];
            firstBuild = build;
        }
        
    }
    
    return groups;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//
/**
 This is here to help the outline view & the tree controller to work together
 **/
- (NSString*)name
{
    return self.identifier;
}

- (NSArray*)transactions
{
    return [[self.tprs valueForKey:@"transaction"] allObjects];
}

- (BOOL)isLeaf
{
    return YES;
}

- (BOOL)isGroup
{
    return NO;
}

- (NSInteger)outlineCount
{
    return 0;
}

- (NSArray*)outlineChildren
{
    return @[];
}

//
//////////////////////////////////////////////////////////////////////////////////////////////////

@end
