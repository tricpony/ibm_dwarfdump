//
//  Configuration.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/27/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "Configuration.h"
#import "CoreDataUtility.h"

@implementation Configuration

+ (Configuration*)myConfigurationWithEditContext:(NSManagedObjectContext*)ctx
{
    Configuration *config = nil;
    
    if (!ctx) return nil;
    config = [[CoreDataUtility sharedInstance] fetchConfigurationWithEditContext:ctx];
    if (!config) {
        config = [[CoreDataUtility sharedInstance] insertNewConfigurationWithEditContext:ctx];
        config.lastVisitedBuildPath = NSHomeDirectory();
        config.lastVisitedTPRPath = NSHomeDirectory();
        config.lastVisitedPayloadPath = NSHomeDirectory();
    }
    
    return config;
}

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    [self performSelector:@selector(setCreateDate:) withObject:[NSDate date]];
}

@end
