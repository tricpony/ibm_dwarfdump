//
//  TPR.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "TPR.h"
#import "Build.h"
#import "Transaction.h"
#import "CoreDataUtility.h"

@implementation TPR

+ (TPR*)createTPRWithZipFilename:(NSString*)zipFilename build:(Build*)build withEditContext:(NSManagedObjectContext*)ctx
{
    TPR *nextTPR = nil;
    
    nextTPR = [[CoreDataUtility sharedInstance] fetchTPRMatchingZipFilename:zipFilename build:build withEditContext:ctx];
    if (!nextTPR) {
        nextTPR = [[CoreDataUtility sharedInstance] insertNewTPRWithEditContext:ctx];
        nextTPR.zipFilename = zipFilename;
    }
    
    return nextTPR;
}

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    //all eos have a create date so set it here
    [self performSelector:@selector(setCreateDate:) withObject:[NSDate date]];
}

- (NSString*)buildPath
{
    return self.build.path;
}

- (NSString*)payloadPath
{
    return self.transaction.payloadPath;
}

- (NSString*)buildIdentifier
{
    return self.build.identifier;
}

@end
