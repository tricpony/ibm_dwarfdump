//
//  TPR.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Build, Transaction;

NS_ASSUME_NONNULL_BEGIN

@interface TPR : NSManagedObject

+ (TPR*)createTPRWithZipFilename:(NSString*)zipFilename build:(Build*)build withEditContext:(NSManagedObjectContext*)ctx;

@end

NS_ASSUME_NONNULL_END

#import "TPR+CoreDataProperties.h"
