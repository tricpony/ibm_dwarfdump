//
//  Transaction.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "Transaction.h"
#import "Build.h"
#import "TPR.h"
#import "CoreDataUtility.h"
#import "NSDate+Factory.h"
#import "NSColor+Factory.h"

const char* statusStrings[] = {"PASSED","FAILED","PENDING"};

@implementation Transaction

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    //all eos have a create date so set it here
    [self performSelector:@selector(setCreateDate:) withObject:[NSDate date]];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//
/**
 This is here to help the outline view & the tree controller to work together
 **/
- (NSString*)name
{
    return self.buildIdentifier;
}

- (BOOL)isLeaf
{
    return YES;
}

- (BOOL)isGroup
{
    return NO;
}

- (NSInteger)outlineCount
{
    return 0;
}

- (NSArray*)outlineChildren
{
    return @[];
}
//
//////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString*)buildIdentifier
{
    return self.tpr.build.identifier;
}

- (NSString*)tprPath
{
    return self.tpr.path;
}

- (NSString*)buildPath
{
    return self.tpr.build.path;
}

- (NSString*)tprZipFilenameWithExtension
{
    return self.tpr.zipFilename;
}

- (NSString*)tprZipFilename
{
    return [self.tpr.zipFilename stringByDeletingPathExtension];
}

- (NSArray*)tprs
{
    return [self.tpr.build.tprs allObjects];
}

- (FactoryExecutionStatus)executionStatusValue
{
    return [self.executionType intValue];
}

- (NSString*)executionStatusString
{
    NSString *status;
    
    status = [NSString stringWithFormat:@"%s",statusStrings[self.executionStatusValue]];
    return status;
}

#pragma mark - Text Color

- (NSColor*)executionStatusColor
{
    NSColor *color = [NSColor blackColor];
    
    if (self.executionStatusValue == kExecutedFail) {
        color = [NSColor orangeColor];
    }else if (self.executionStatusValue == kNeverExecuted) {
        color = [NSColor acgmeDarkGreen];
    }else if (self.executionStatusValue == kExecutedPass) {
        color = [NSColor redColor];
    }
    
    return color;
}

/**
 Returns an ordered collection of grouped transactions
 **/
+ (NSArray*)groupedTransactionsByCreateDateWithEditContext:(NSManagedObjectContext*)ctx
{
    NSArray *allTransactions;
    Transaction *firstTransaction;
    NSMutableArray *groups = nil;
    NSMutableDictionary *group = nil;
    NSMutableArray *groupTransactions = nil;
    NSString *datePattern = @"MMM d yy";
    
    //these have already been sorted in descending order which means the most recent transaction will be first and oldest last
    allTransactions = [[CoreDataUtility sharedInstance] fetchAllTransactionsWithEditContext:ctx];
    firstTransaction = [allTransactions firstObject];
    groups = [NSMutableArray arrayWithCapacity:1];

    for (Transaction *trans in allTransactions) {
        
        if (!groupTransactions) {
            
            group = [NSMutableDictionary dictionaryWithCapacity:5];
            [groups addObject:group];
            groupTransactions = [NSMutableArray arrayWithCapacity:5];
            group[@"date_label"] = [trans.createDate formattedDateForPattern:datePattern];
            group[@"transactions"] = groupTransactions;
            [groupTransactions addObject:trans];
            firstTransaction = trans;
            
        }else if ([firstTransaction.createDate isSameDayOfYear:trans.createDate]) {
            
            [groupTransactions addObject:trans];
            
        }else{
            groupTransactions = nil;
        }

    }
    
    return groups;
}

@end
