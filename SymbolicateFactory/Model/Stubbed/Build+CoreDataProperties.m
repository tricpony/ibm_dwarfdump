//
//  Build+CoreDataProperties.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Build+CoreDataProperties.h"

@implementation Build (CoreDataProperties)

@dynamic createDate;
@dynamic identifier;
@dynamic path;
@dynamic hasArchive;
@dynamic tprs;

@end
