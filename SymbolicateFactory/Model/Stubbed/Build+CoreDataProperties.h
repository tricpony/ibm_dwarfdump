//
//  Build+CoreDataProperties.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Build.h"

NS_ASSUME_NONNULL_BEGIN

@interface Build (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createDate;
@property (nullable, nonatomic, retain) NSString *identifier;
@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSNumber *hasArchive;
@property (nullable, nonatomic, retain) NSSet<TPR *> *tprs;

@end

@interface Build (CoreDataGeneratedAccessors)

- (void)addTprsObject:(TPR *)value;
- (void)removeTprsObject:(TPR *)value;
- (void)addTprs:(NSSet<TPR *> *)values;
- (void)removeTprs:(NSSet<TPR *> *)values;

@end

NS_ASSUME_NONNULL_END
