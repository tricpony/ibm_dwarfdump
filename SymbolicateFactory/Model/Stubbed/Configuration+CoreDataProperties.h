//
//  Configuration+CoreDataProperties.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/27/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Configuration.h"

NS_ASSUME_NONNULL_BEGIN

@interface Configuration (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createDate;
@property (nullable, nonatomic, retain) NSString *lastVisitedTPRPath;
@property (nullable, nonatomic, retain) NSString *lastVisitedBuildPath;
@property (nullable, nonatomic, retain) NSString *lastVisitedPayloadPath;

@end

NS_ASSUME_NONNULL_END
