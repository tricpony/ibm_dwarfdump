//
//  TPR+CoreDataProperties.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TPR.h"

NS_ASSUME_NONNULL_BEGIN

@interface TPR (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createDate;
@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSString *sandbox;
@property (nullable, nonatomic, retain) NSString *zipFilename;
@property (nullable, nonatomic, retain) Build *build;
@property (nullable, nonatomic, retain) Transaction *transaction;

@end

NS_ASSUME_NONNULL_END
