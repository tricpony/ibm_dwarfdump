//
//  Transaction+CoreDataProperties.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Transaction.h"

NS_ASSUME_NONNULL_BEGIN

@interface Transaction (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createDate;
@property (nullable, nonatomic, retain) NSNumber *executionType;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *payloadPath;
@property (nullable, nonatomic, retain) NSString *sandbox;
@property (nullable, nonatomic, retain) TPR *tpr;

@end

NS_ASSUME_NONNULL_END
