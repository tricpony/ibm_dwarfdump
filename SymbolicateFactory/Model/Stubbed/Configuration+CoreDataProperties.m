//
//  Configuration+CoreDataProperties.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/27/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Configuration+CoreDataProperties.h"

@implementation Configuration (CoreDataProperties)

@dynamic createDate;
@dynamic lastVisitedTPRPath;
@dynamic lastVisitedBuildPath;
@dynamic lastVisitedPayloadPath;

@end
