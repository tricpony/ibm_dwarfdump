//
//  Transaction+CoreDataProperties.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Transaction+CoreDataProperties.h"

@implementation Transaction (CoreDataProperties)

@dynamic createDate;
@dynamic executionType;
@dynamic name;
@dynamic payloadPath;
@dynamic sandbox;
@dynamic tpr;

@end
