//
//  main.m
//  DwarfDump
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 ANTHONY B ARTHUR. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
