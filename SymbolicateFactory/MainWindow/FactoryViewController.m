//
//  FactoryViewController.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

#import "FactoryViewController.h"
#import "TaskManager.h"
#import "TextFieldDrag.h"
#import "ConstantsDefines.h"
#import "CoreDataUtility.h"
#import "Transaction.h"
#import "Configuration.h"
#import "GroupHeaderRowItem.h"
#import "Build.h"

@interface FactoryViewController()
@property (weak) IBOutlet NSBox *box;
@property (weak) IBOutlet TextFieldDrag *tprPathTextField;
@property (weak) IBOutlet TextFieldDrag *buildPathTextField;
@property (nonatomic, strong) TaskManager *taskMgr;
@property (nonatomic, strong) Configuration *config;
@property (nonatomic, strong) NSManagedObjectContext *localEditContext;
@property (weak) IBOutlet NSTableView *tableView;
@property (strong) IBOutlet NSArrayController *transactionArrayController;
@property (strong) IBOutlet NSTreeController *treeController;

@property (nonatomic, strong) NSArray *rootTreeControllerItems;
- (IBAction)openBuildPath:(id)sender;

@end

@implementation FactoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.box.wantsLayer = YES;
    CALayer *layer = self.box.layer;
    
    layer.borderColor = [NSColor blackColor].CGColor;
    layer.borderWidth = 1;
    layer.cornerRadius = 6.25;
    
    self.tprPathTextField.stringValue = @"/Users/aarthur/Desktop/IR3-TPRs-151012/irisa-trav01/lln_iris";
    self.buildPathTextField.stringValue = @"/Users/aarthur/Desktop/NTI20_Builds";
    self.localEditContext = APP_DELEGATE.managedObjectContext;
    [self setUpRootTreeControllerItems];
//    [self deleteAllBuilds];
    self.config = [Configuration myConfigurationWithEditContext:self.localEditContext];
}

- (void)setRepresentedObject:(id)representedObject
{
    [super setRepresentedObject:representedObject];
}

- (BOOL)validateConfiguration
{
    return YES;
}

- (void)deleteAllBuilds
{
    NSArray *defs;
    int i = 0;
    
    defs = [[CoreDataUtility sharedInstance] fetchAllBuildsWithEditContext:self.localEditContext];
    while (i < [defs count]) {
        Transaction *def = defs[i++];
        
        [self.localEditContext deleteObject:def];
    }
    [APP_DELEGATE saveAction:nil];
}

- (void)runFactory:(id)sender
{
    NSString *pathComponent;
    
    pathComponent = @"com.ibm.symbolicateFactory";

    self.taskMgr = [[TaskManager alloc] init];
    if ([self.tableView selectedRow] != -1) {
        Transaction *transDef;
        
        transDef = [self.transactionArrayController arrangedObjects][[self.tableView selectedRow]];
        [self.taskMgr executeTransaction:transDef completionBlock:^(NSError *error) {
            
           [self setUpRootTreeControllerItems];
            
        }];
        
    }else if ([self validateConfiguration]) {
        self.taskMgr.sandboxPath = [NSTemporaryDirectory() stringByAppendingPathComponent:pathComponent];
        self.taskMgr.vc = self;
        [self.taskMgr executeSearchForMatchingBuildsFromTPRFolder:self.tprPathTextField.stringValue
                                                    toBuildFolder:self.buildPathTextField.stringValue
                                                  completionBlock:^(NSError *error){
                                                  
                                                      [self setUpRootTreeControllerItems];
                                                  
                                                  }];
    }else{
        self.taskMgr = nil;
    }
}

- (void)logToConsole:(NSString*)message
{
}

#pragma mark - Open Panel

- (IBAction)chooseTPRPath:(id)sender
{
    NSOpenPanel *openPanel = nil;
    
    openPanel = [NSOpenPanel openPanel];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanChooseFiles:NO];
    [openPanel setCanCreateDirectories:YES];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel setDirectoryURL:[NSURL fileURLWithPath:self.config.lastVisitedTPRPath]];
    
    if ([openPanel runModal] == NSModalResponseOK) {
        self.config.lastVisitedTPRPath = [[openPanel URL] path];
        [APP_DELEGATE saveAction:nil];
    }
}

- (IBAction)chooseBuildPath:(id)sender
{
    NSOpenPanel *openPanel = nil;
    
    openPanel = [NSOpenPanel openPanel];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanChooseFiles:NO];
    [openPanel setCanCreateDirectories:YES];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel setDirectoryURL:[NSURL fileURLWithPath:self.config.lastVisitedBuildPath]];
    
    if ([openPanel runModal] == NSModalResponseOK) {
        self.config.lastVisitedBuildPath = [[openPanel URL] path];
        [APP_DELEGATE saveAction:nil];
    }
}

#pragma mark - Sort Descriptors

- (NSArray*)buildSortDescriptor
{
    NSSortDescriptor *descriptor;
    
    descriptor = [NSSortDescriptor sortDescriptorWithKey:@"buildIdentifier" ascending:YES];
    return @[descriptor];
}

- (NSArray*)transactionSortDescriptor
{
    NSSortDescriptor *descriptor;
    
    descriptor = [NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO];
    return @[descriptor];
}

#pragma mark Outline view to TreeController bindings support

- (NSArrayController*)arrayControllerForObjects:(NSArray*)transactions
{
    NSArrayController *nextArrayController;
    
    nextArrayController = [[NSArrayController alloc] initWithContent:transactions];
    nextArrayController.sortDescriptors = [self transactionSortDescriptor];

    return nextArrayController;
}

- (void)setUpRootTreeControllerItems
{
    NSArray *buildGroups;
    NSMutableArray *treeBranches = nil;
    GroupHeaderRowItem *rootDateLabelGroupItem = nil;

    buildGroups = [Build groupedBuildsByCreateDateWithEditContext:self.localEditContext];
    for (NSDictionary *group in buildGroups) {
        
        if (!treeBranches) treeBranches = [NSMutableArray arrayWithCapacity:[buildGroups count]];
        
        rootDateLabelGroupItem = [GroupHeaderRowItem itemWithTitle:group[@"date_label"]];
        rootDateLabelGroupItem.childItemsArrayController = [self arrayControllerForObjects:group[@"builds"]];
        [treeBranches addObject:rootDateLabelGroupItem];
    }
    
    self.rootTreeControllerItems = treeBranches;
}

- (IBAction)openBuildPath:(id)sender
{
    Transaction *transaction;
    NSString *path;
    
    transaction = [[self.transactionArrayController selectedObjects] lastObject];
    path = transaction.buildPath;
    [[NSWorkspace sharedWorkspace] openFile:path];
}

@end
