//
//  GroupHeaderRowItem.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

#import "GroupHeaderRowItem.h"
#import "NSColor+Factory.h"

@interface GroupHeaderRowItem()

@end

@implementation GroupHeaderRowItem

- (void)dealloc
{
    [self.childItemsArrayController removeObserver:self forKeyPath:@"arrangedObjects"];
}

+ (GroupHeaderRowItem*)itemWithTitle:(NSString*)title;
{
    GroupHeaderRowItem *group = [[[self class] alloc] init];
    group.name = title;
    return group;
}

- (id)init
{
    if (self = [super init]) {
        self.isGroup = YES;
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"arrangedObjects"]) {
        
        [self willChangeValueForKey:@"outlineChildren"];
        [self didChangeValueForKey:@"outlineChildren"];
    }
}

#pragma mark Encoding
//to support autosave expanded outlineView rows

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super init];
    self.name = [coder decodeObjectForKey:@"name"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder*)coder
{
    [coder encodeObject:self.name forKey:@"name"];
}

#pragma mark Outline view bindings support

- (void)setOutlineChildren:(NSArray *)outlineChildren
{
}

- (NSArray*)outlineChildren
{
    return self.childItemsArrayController.arrangedObjects;
}

- (NSImage*)image
{
    return nil;
}

- (BOOL)imageIsHidden
{
    return YES;
}

- (BOOL)isLeaf
{
    return [self.childItemsArrayController.arrangedObjects count] == 0;
}

- (NSInteger)outlineCount
{
    return [self.childItemsArrayController.arrangedObjects count];
}

- (NSString*)zipFilename
{
    return @"";
}

- (NSString*)buildPath
{
    return @"";
}

- (NSString*)payloadPath
{
    return @"";
}

- (NSArray*)transactions
{
    return @[];
}

@end
