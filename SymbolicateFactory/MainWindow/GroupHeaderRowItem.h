//
//  GroupHeaderRowItem.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface GroupHeaderRowItem : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSArrayController *childItemsArrayController;
@property (nonatomic, assign) BOOL isGroup;

+ (GroupHeaderRowItem*)itemWithTitle:(NSString*)title;

@end
