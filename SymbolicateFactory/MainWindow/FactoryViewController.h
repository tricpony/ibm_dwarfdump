//
//  FactoryViewController.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface FactoryViewController : NSViewController

- (void)runFactory:(id)sender;
- (void)logToConsole:(NSString*)message;

@end

