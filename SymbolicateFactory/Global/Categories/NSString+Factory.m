//
//  NSString+Factory.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/29/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "NSString+Factory.h"

@implementation NSString (Factory)

- (NSString*)safeSubstringWithRange:(NSRange)range
{
    if ([self length] < range.length) {
        return nil;
    }
    
    return [self substringWithRange:range];
}

/**
 Only accepts the operation when [self length] == 7
 Subdivides self into 3 pairs and converts each from hex to base 10
 **/
- (NSMutableArray*)hexAsRGB
{
    NSMutableArray *rgb = nil;
    
    //there must be 3 pairs
    if ([self length] < 7) {
        return nil;
    }
    NSString *redString = nil;
    NSString *greenString = nil;
    NSString *blueString = nil;
    NSRange r;
    char* endPtr = NULL;
    const char* s;
    long l;
    
    rgb = [NSMutableArray arrayWithCapacity:1];
    //parse red component, starting at index 1 to strip off #
    r.location = 1;
    r.length = 2;
    redString = [NSString stringWithFormat:@"0x%@",[self substringWithRange:r]];
    
    //parse blue component
    r.location = 3;
    r.length = 2;
    greenString = [NSString stringWithFormat:@"0x%@",[self substringWithRange:r]];
    
    //green component
    r.location = 5;
    r.length = 2;
    blueString = [NSString stringWithFormat:@"0x%@",[self substringWithRange:r]];
    
    s = [redString UTF8String];
    l = strtol(s, &endPtr, 0);
    if (s != endPtr && *endPtr == '\0') {
        [rgb addObject:@(l)];
    }
    
    endPtr = NULL;
    s = [greenString UTF8String];
    l = strtol(s, &endPtr, 0);
    if (s != endPtr && *endPtr == '\0') {
        [rgb addObject:@(l)];
    }
    
    endPtr = NULL;
    s = [blueString UTF8String];
    l = strtol(s, &endPtr, 0);
    if (s != endPtr && *endPtr == '\0') {
        [rgb addObject:@(l)];
    }
    return rgb;
}

@end
