//
//  NSDate+Factory.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Factory)

- (NSInteger)year;
- (NSInteger)month;
- (NSInteger)day;
- (NSDate*)midnight;
- (NSDate*)dateByAddingDays:(NSInteger)days;
- (BOOL)isGreaterThanOrEqualTo:(NSDate*)aDate;
- (BOOL)isLessThan:(NSDate*)aDate;
- (NSString*)formattedDateForPattern:(NSString*)pattern;
- (BOOL)isSameDayOfYear:(NSDate*)aDate;

@end
