//
//  NSString+Factory.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/29/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Factory)

- (NSString*)safeSubstringWithRange:(NSRange)range;
- (NSMutableArray*)hexAsRGB;

@end
