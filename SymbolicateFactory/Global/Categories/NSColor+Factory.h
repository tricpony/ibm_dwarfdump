//
//  NSColor+Factory.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/29/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSColor (Factory)

+ (NSColor*)colorForHexRGB:(NSString*)rgbHexString withAlpha:(CGFloat)alpha;
+ (NSColor*)colorForHexRGB:(NSString*)rgbHexString;
+ (NSColor*)acgmeDarkGreen;
+ (NSColor*)ibmBlueColor;
+ (NSColor*)slateGrayColor;

@end
