//
//  NSColor+Factory.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/29/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "NSColor+Factory.h"
#import "NSString+Factory.h"

@implementation NSColor (Factory)

+ (NSColor*)colorForHexRGB:(NSString*)rgbHexString withAlpha:(CGFloat)alpha
{
    NSArray *rgb = nil;
    CGFloat red,green,blue;
    NSColor *aColor = nil;
    CGFloat denom = 255.0;
    
    rgb = [rgbHexString hexAsRGB];
    
    if (rgb) {
        
        //we want the color as a percent, so divide by 255.0
        red = ((CGFloat)[rgb[0] longValue])/denom;
        green = ((CGFloat)[rgb[1] longValue])/denom;
        blue = ((CGFloat)[rgb[2] longValue])/denom;
        aColor = [NSColor colorWithRed:red green:green blue:blue alpha:alpha];
    }
    return aColor;
}

+ (NSColor*)colorForHexRGB:(NSString*)rgbHexString
{
    return [self colorForHexRGB:rgbHexString withAlpha:1.0];
}

/**
 components for DarkGreen, #006400
 http://www.blooberry.com/indexdot/color/x11makerFrameNS.htm
 **/
+ (NSColor*)acgmeDarkGreen
{
    return [self colorForHexRGB:@"#006400"];
}

+ (NSColor*)ibmBlueColor
{
    return [self colorForHexRGB:@"#006699"];
}

+ (NSColor*)slateGrayColor
{
    return [self colorForHexRGB:@"#708090"];
}

@end
