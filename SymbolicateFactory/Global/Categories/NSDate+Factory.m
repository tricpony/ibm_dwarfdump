//
//  NSDate+Factory.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/28/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "NSDate+Factory.h"

@implementation NSDate (Factory)

- (NSInteger)year
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned unitFlags = NSCalendarUnitYear;
    NSDateComponents *rtOperand = [gregorian components:unitFlags fromDate:self];
    
    return [rtOperand year];
}

- (NSInteger)month
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned unitFlags = NSCalendarUnitMonth;
    NSDateComponents *rtOperand = [gregorian components:unitFlags fromDate:self];
    
    return [rtOperand month];
}

- (NSInteger)day
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned unitFlags = NSCalendarUnitDay;
    NSDateComponents *rtOperand = [gregorian components:unitFlags fromDate:self];
    
    return [rtOperand month];
}

/**
 Returns midnight of the reciever
 **/
- (NSDate*)midnight
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *nowComps = nil;
    NSDate *midnight = nil;
    
    nowComps = [gregorian components:unitFlags fromDate:self];
    [offsetComponents setYear:[nowComps year]];
    [offsetComponents setMonth:[nowComps month]];
    [offsetComponents setDay:[nowComps day]];
    [offsetComponents setHour:0];
    [offsetComponents setMinute:0];
    [offsetComponents setSecond:0];
    midnight = [gregorian dateFromComponents:offsetComponents];
    
    return midnight;
}

/**
 Return date object set to day of today but year and month
 is set to receiver's month + months.
 **/
- (NSDate*)dateByAddingDays:(NSInteger)days
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *resultDate = nil;
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    
    [offsetComponents setDay:days];
    resultDate = [gregorian dateByAddingComponents:offsetComponents toDate:self options:0];
    
    return resultDate;
}

/**
 Return YES if the receiver is same as or after aDate
 **/
- (BOOL)isGreaterThanOrEqualTo:(NSDate*)aDate
{
    return [self compare:aDate] == NSOrderedDescending || [self compare:aDate] == NSOrderedSame;
}

/**
 Return YES if the receiver is before aDate
 **/
- (BOOL)isLessThan:(NSDate*)aDate
{
    return [self compare:aDate] == NSOrderedAscending;
}

- (NSString*)formattedDateForPattern:(NSString*)pattern
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *result = nil;
    NSDate *today = [NSDate date];
    NSDate *yesterday = [today dateByAddingDays:-1];
    
    if ([self isSameDayOfYear:yesterday]) {
        return @"Yesterday";
    }
    if ([self isSameDayOfYear:today]) {
        return @"Today";
    }
    
    [formatter setDateFormat:pattern];
    result = [formatter stringFromDate:self];
    return result;
}

/**
 Return YES if the receiver has the same day, month, & year as aDate
 **/
- (BOOL)isSameDayOfYear:(NSDate*)aDate
{
    if (aDate == nil) return NO;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *ltOperand = [gregorian components:unitFlags fromDate:self];
    NSDateComponents *rtOperand = [gregorian components:unitFlags fromDate:aDate];
    BOOL isSame;
    
    isSame = [ltOperand year] == [rtOperand year] && [ltOperand month] == [rtOperand month] && [ltOperand day] == [rtOperand day];
    
    return isSame;
}

@end
