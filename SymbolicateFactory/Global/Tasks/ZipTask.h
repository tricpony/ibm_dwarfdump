//
//  ZipTask.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/23/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "TaskBase.h"

@interface ZipTask : TaskBase
@property (nonatomic, strong) NSString *destinationPath;

@end
