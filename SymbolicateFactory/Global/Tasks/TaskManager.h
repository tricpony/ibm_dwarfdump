//
//  TaskManager.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Transaction;
@class FactoryViewController;
typedef void(^TaskaManagerCompletionBlock)(NSError* error);

@interface TaskManager : NSObject
@property (nonatomic, strong) NSDictionary *stdOutCache;
@property (nonatomic, strong) NSString *sandboxPath;
@property (nonatomic, strong) NSString *finalDestinationPath;
@property (nonatomic, assign) int nextVersionNbr;
@property (nonatomic, strong) NSString *sourcePath;
@property (nonatomic, strong) NSString *pathComponentForTempFolder;
@property (nonatomic, strong) FactoryViewController *vc;

- (void)executeTransaction:(Transaction*)aTransaction completionBlock:(TaskaManagerCompletionBlock)block;
- (void)executeSearchForMatchingBuildsFromTPRFolder:(NSString*)tprFolder toBuildFolder:(NSString*)buildFolder completionBlock:(TaskaManagerCompletionBlock)block;

@end
