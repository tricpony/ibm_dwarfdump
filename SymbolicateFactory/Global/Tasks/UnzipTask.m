//
//  UnzipTask.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/21/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "UnzipTask.h"

@interface UnzipTask()
@property (nonatomic, strong) NSTask *unzipTask;

@end

@implementation UnzipTask

- (void)performTaskForCompletionBlock:(TaskCompletionBlock)block
{
    NSDictionary *defaultEnvironment = [[NSProcessInfo processInfo] environment];
    NSMutableDictionary *environment = [defaultEnvironment mutableCopy];
    
    environment[@"NSUnbufferedIO"] = @"YES";

    [super performTaskForCompletionBlock:block];
    
    self.unzipTask = [[NSTask alloc] init];
    [self.unzipTask setLaunchPath:@"/usr/bin/unzip"];
    [self.unzipTask setArguments:@[@"-q",self.sourcePath,@"-d",self.sandboxPath]];
    [self.unzipTask setEnvironment:environment];

    NSPipe *pipe = [NSPipe pipe];
    [self.unzipTask setStandardInput:pipe];
    [self.unzipTask setStandardOutput:pipe];
    [self.unzipTask setStandardError:pipe];
    [self launchTask:self.unzipTask];
}

@end
