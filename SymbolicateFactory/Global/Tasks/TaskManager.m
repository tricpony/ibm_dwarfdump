//
//  TaskManager.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.
//

/**
 Core codesigning logic taken from:
 https://github.com/maciekish/iReSign.git
 **/

#import "TaskManager.h"
#import "Build.h"
#import "TPR.h"
#import "Transaction.h"
#import "CoreDataUtility.h"
#import "ConstantsDefines.h"
#import "SymbolicateTask.h"
#import "UnzipTask.h"
#import "ZipTask.h"
#import "FactoryViewController.h"
#import "NSString+Factory.h"

static NSString *kKeyBundleIDPlistApp               = @"CFBundleIdentifier";
static NSString *kKeyBundleVersonPlistApp           = @"CFBundleVersion";
static NSString *kKeyBundleIDPlistiTunesArtwork     = @"softwareVersionBundleId";
static NSString *kKeyInfoPlistApplicationProperties = @"ApplicationProperties";
static NSString *kKeyInfoPlistApplicationPath       = @"ApplicationPath";
static NSString *kPayloadDirName                    = @"Payload";
static NSString *kProductsDirName                   = @"Products";
static NSString *kInfoPlistFilename                 = @"Info.plist";
static NSString *kiTunesMetadataFileName            = @"iTunesMetadata";

@interface TaskManager()
@property (nonatomic, assign) __block int timesExecutedSoFar;
@property (nonatomic, strong) UnzipTask *unZipTask;
@property (nonatomic, strong) SymbolicateTask *symbolicateTask;
@property (nonatomic, strong) Transaction *transaction;
@property (nonatomic, strong) NSMutableArray *tasks;
@property (nonatomic, strong) TaskaManagerCompletionBlock completionBlock;

@end

@implementation TaskManager

- (void)executeTransaction:(Transaction*)aTransaction completionBlock:(TaskaManagerCompletionBlock)block
{
    NSAssert2(aTransaction, @"Task manager failed: %@ %@", @"nil transaction not allowed in: ", NSStringFromSelector(_cmd));
    
    self.transaction = aTransaction;
    self.completionBlock = block;
    [self performTransaction];
}

- (void)performTransaction
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSString *payloadPath;
    NSString *payloadFilename;
    NSString *dSYMPath;
    NSArray *allCrashFiles = nil;
    
    payloadPath = [NSString stringWithFormat:@"%@/%@_Payload",[self.transaction.tpr.path stringByDeletingLastPathComponent],self.transaction.tprZipFilename];
    self.transaction.payloadPath = payloadPath;
    
    if ([fm fileExistsAtPath:payloadPath]) {
        [fm removeItemAtPath:payloadPath error:&error];
    }
    [fm createDirectoryAtPath:payloadPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    allCrashFiles = [self allCrashLogFilenamesAtPath:self.transaction.sandbox];
    
    for (NSString *crashFilePath in allCrashFiles) {
        NSString *nextSymbolicatedFilename;
        
        @autoreleasepool {
            nextSymbolicatedFilename = [self nextSymbolicatedFilenameForCrashLog:[crashFilePath lastPathComponent]];
            payloadFilename = [NSString stringWithFormat:@"%@/%@",payloadPath,nextSymbolicatedFilename];
            self.symbolicateTask = [[SymbolicateTask alloc] init];
            self.symbolicateTask.outputPath = payloadFilename;
            self.symbolicateTask.sandboxPath = @".";
            
            if (!self.tasks) {
                self.tasks = [NSMutableArray arrayWithCapacity:4];
            }
            [self.tasks addObject:self.symbolicateTask];
            
            self.symbolicateTask.crashLogPath = crashFilePath;
            
            dSYMPath = [self dSYMPathAtPath:self.transaction.buildPath];
            if (dSYMPath) {
                self.symbolicateTask.dSYMPath = dSYMPath;
                
                [self.symbolicateTask performTaskForCompletionBlock:^(NSError *error, NSTask *task) {
                    if (!error) {
                        self.transaction.executionType = @(kExecutedPass);
                    }
                }];
            }
        }
    }

    [APP_DELEGATE.managedObjectContext save:&error];
    self.symbolicateTask = nil;
    self.tasks = nil;
    self.completionBlock(error);
    self.completionBlock = nil;

}

- (NSString*)nextSymbolicatedFilenameForCrashLog:(NSString*)crashLogFilename
{
    NSScanner *scanner;
    NSString *crashID;
    NSString *nextFilename = nil;
    
    scanner = [NSScanner scannerWithString:crashLogFilename];
    if ([scanner scanString:@"verse." intoString:NULL]) {
        
        [scanner scanUpToString:@".crash" intoString:&crashID];
        
        if (!crashID || ![crashID length]) {
            crashID = @"IshKaBibble-Crash_ID_Number_Missing";
        }
        
        nextFilename = [NSString stringWithFormat:@"Factory.%@.crash",crashID];
        
    }
    
    return nextFilename;
}

- (NSArray*)allCrashLogFilenamesAtPath:(NSString*)path
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *folderContents;
    NSMutableArray *crashFiles = nil;;
    
    folderContents = [fm subpathsAtPath:path];

    for (NSString *filename in folderContents) {
        
        if ([[filename pathExtension] isEqualToString:@"crash"]) {
            
            if (!crashFiles) {
                crashFiles = [NSMutableArray arrayWithCapacity:10];
            }
            [crashFiles addObject:[NSString stringWithFormat:@"%@/%@",path,filename]];
        }
    }
    return crashFiles;
}

- (NSString*)extract_dSYMatPath:(NSString*)path
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *folderContents;
    NSString *dSYMPath = nil;

    folderContents = [fm subpathsAtPath:path];
    
    for (NSString *filename in folderContents) {
        NSString *firstThreeLetters;
        NSRange range;
        
        range.location = 0;
        range.length = 3;
        firstThreeLetters = [[filename lastPathComponent] safeSubstringWithRange:range];
        if ([firstThreeLetters isEqualToString:@"IBM"] && [[filename pathExtension] isEqualToString:@"dSYM"]) {
            
            //found it!
            dSYMPath = [NSString stringWithFormat:@"%@/%@",path,filename];
            
        }
        
    }
    
    return dSYMPath;
}

- (NSString*)dSYMPathAtPath:(NSString*)path
{
    __block Build *build = self.transaction.tpr.build;
    __block NSString *dSYMPath = nil;
    
    if ([build.hasArchive boolValue]) {
        return [self extract_dSYMatPath:path];
    }else{
        NSFileManager *fm = [NSFileManager defaultManager];
        NSString *searchPath;
        
        //bummer, there's not an archive already here, now search for either an ipa or zip file and unzip it
        
        //first choice, look for something called Verse.xcarchive.zip
        searchPath = [NSString stringWithFormat:@"%@/Verse.xcarchive.zip",path];
        if ([fm fileExistsAtPath:searchPath]) {
            
            //Yippie!! there it is - now what do we do?
            self.unZipTask = (id)[[UnzipTask alloc] init];
            self.unZipTask.sandboxPath = path;
            self.unZipTask.sourcePath = searchPath;
            
            [self.unZipTask performTaskForCompletionBlock:^(NSError *error, NSTask *task) {
                build.hasArchive = @(YES);
                dSYMPath = [self extract_dSYMatPath:path];
            }];
            
        }
        //second choice, look for something called Verse.ipa -- which is actually a zip file with an ipa extension -- this was never tested!!
        searchPath = [NSString stringWithFormat:@"%@/Verse.xcarchive.zip",path];
        if ([fm fileExistsAtPath:searchPath]) {
            
            //Yippie!! there it is - now what do we do?
            self.unZipTask = (id)[[UnzipTask alloc] init];
            self.unZipTask.sandboxPath = path;
            self.unZipTask.sourcePath = searchPath;
            
            [self.unZipTask performTaskForCompletionBlock:^(NSError *error, NSTask *task) {
                build.hasArchive = @(YES);
                dSYMPath = [self extract_dSYMatPath:path];
            }];
            
        }
        
    }
    self.unZipTask = nil;
    
    return dSYMPath;
}

/**
 Algorithm
 
 1 - loop over TPR’s
 2 - unzip it in safe folder
 3 - look for the tpr.html
 4 - parse out build number
 5 - search for matching build numbered folder in build path
 6 - add Transaction to table view
 
 These steps are performed for each individual match from #6 by the user tapping a button
 7 - open archive and look for dSYM file
 8 - move .crash files & dSYM file into new folder NSTemporaryDirectory()
 9 - run the symbolicate script and output results to the same folder as #7
 
 if there are more than 3 TPR zip files then
 divide the TPR's in half and process them in 2 GCD threads
 
 **/
- (void)executeSearchForMatchingBuildsFromTPRFolder:(NSString*)tprFolder toBuildFolder:(NSString*)buildFolder completionBlock:(TaskaManagerCompletionBlock)block
{
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *folderContents;
    NSError* error = nil;
    
    folderContents = [fm subpathsAtPath:tprFolder];
    @autoreleasepool {
        [self processTPRsIn:folderContents tpr:tprFolder buildPath:buildFolder];
    }
    [APP_DELEGATE.managedObjectContext save:&error];
    self.unZipTask = nil;
    self.tasks = nil;
    block(error);
}

- (void)processTPRsIn:(NSArray*)list tpr:(NSString*)tprFolder buildPath:(NSString*)buildFolder
{
    for (NSString *folderName in list) {
        
        if ([[folderName pathExtension] isEqualToString:@"zip"]) {
            
            NSFileManager *fm = [NSFileManager defaultManager];
            NSError *error = nil;
            NSString *stalePath;
            
            stalePath = [self.sandboxPath stringByAppendingPathComponent:[folderName stringByDeletingPathExtension]];
            if ([fm fileExistsAtPath:stalePath]) {
                [fm removeItemAtPath:stalePath error:&error];
            }
            [fm createDirectoryAtPath:stalePath withIntermediateDirectories:YES attributes:nil error:nil];

            if (!error) {
                self.unZipTask = (id)[[UnzipTask alloc] init];
                self.unZipTask.sandboxPath = stalePath;
                self.unZipTask.sourcePath = [NSString stringWithFormat:@"%@/%@",tprFolder,folderName];
                
                if (!self.tasks) {
                    self.tasks = [NSMutableArray arrayWithCapacity:4];
                }
                [self.tasks addObject:self.unZipTask];
                
                [self.unZipTask performTaskForCompletionBlock:^(NSError *error, NSTask *task) {
                    [self searchForBuildNumber:self.unZipTask buildPath:buildFolder];
                }];
                        
            }
        }
    }
}

/**
 Now search for a file named tpr.html
 **/
- (void)searchForBuildNumber:(TaskBase*)aTask buildPath:(NSString*)buildFolder
{
    NSString *tprPath;
    NSFileManager *fm = [NSFileManager defaultManager];

    tprPath = [NSString stringWithFormat:@"%@/tpr.html",aTask.sandboxPath];
    if ([fm fileExistsAtPath:tprPath]) {
        NSURL *url;
        NSString *html;
        NSError *error = nil;
        
        url = [NSURL fileURLWithPath:tprPath];
        html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
        
        if (!error) {
            NSScanner *scanner;
            
            scanner = [NSScanner scannerWithString:html];
            if ([scanner scanUpToString:@"IBM Verse" intoString:NULL]) {
                NSString *buildNbr;
                
                //now looking for an open paran
                [scanner scanUpToString:@"(" intoString:NULL];
                [scanner scanString:@"(" intoString:NULL];
                [scanner scanUpToString:@")" intoString:&buildNbr];
                
                //now we want to find a folder matching buildNbr at the build path
                [self findMatchingBuildFolder:buildNbr buildPath:buildFolder task:aTask];
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *message;
                    NSFileManager *fm = [NSFileManager defaultManager];
                    NSError *error = nil;
                    
                    message = [NSString stringWithFormat:@"Build number not found in: %@",tprPath];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.vc logToConsole:message];
                    });
                    
                    if ([fm fileExistsAtPath:self.sandboxPath]) {
                        [fm removeItemAtPath:self.sandboxPath error:&error];
                    }
                });
            }
        }
    }

}

- (void)findMatchingBuildFolder:(NSString*)buildNbr buildPath:(NSString*)buildFolder task:(TaskBase*)aTask
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *path;
    BOOL isDir;
    
    path = [NSString stringWithFormat:@"%@/%@",buildFolder,buildNbr];
    if ([fm fileExistsAtPath:path isDirectory:&isDir]) {
        
        //found it!!
        if (isDir) {
            Build *nextBuild;
            TPR *nextTpr;
            Transaction *transaction;
            BOOL hasArchive;
            
            nextBuild = [Build buildForIdentifier:buildNbr withEditContext:APP_DELEGATE.managedObjectContext];
            nextBuild.path = path;
            nextTpr = [TPR createTPRWithZipFilename:[aTask.sourcePath lastPathComponent] build:nextBuild withEditContext:APP_DELEGATE.managedObjectContext];
            transaction = [[CoreDataUtility sharedInstance] insertNewTransactionWithEditContext:APP_DELEGATE.managedObjectContext];
            transaction.tpr = nextTpr;
            [nextBuild addTprsObject:nextTpr];
            nextTpr.path = aTask.sourcePath;
            nextTpr.sandbox = aTask.sandboxPath;
            nextTpr.zipFilename = [nextTpr.path lastPathComponent];
            transaction.sandbox = nextTpr.sandbox;
            
            //now we want to find out if this build already has an archive ready to use or will we need to unzip an ipa to get it
            //this will help us when the user selects a TPR to symbolicate so we don't need to fumble around, we just do the unzip straight away
            path = [path stringByAppendingPathComponent:@"Verse.xcarchive"];
            hasArchive = [fm fileExistsAtPath:path];
            nextBuild.hasArchive = @(hasArchive);
        }
    }
}

- (void)logStandardErrorFromTask:(NSTask*)aTask
{
    NSFileHandle *fileHandle = [aTask.standardError fileHandleForReading];

    if ([fileHandle.availableData length]) {
        NSLog(@"Standard Error: %@",[fileHandle readDataToEndOfFile]);
    }
}

@end
