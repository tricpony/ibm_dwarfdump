//
//  TaskBase.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/21/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^TaskCompletionBlock)(NSError* error, NSTask *task);

@interface TaskBase : NSObject
@property (nonatomic, strong) NSString *sandboxPath;
@property (nonatomic, strong) NSString *sourcePath;

- (void)performTaskForCompletionBlock:(TaskCompletionBlock)block;
- (void)launchTask:(NSTask*)aTask;

@end
