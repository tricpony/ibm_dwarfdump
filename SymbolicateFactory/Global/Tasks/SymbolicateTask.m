//
//  SymbolicateTask.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/23/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "SymbolicateTask.h"

/**
 Usage:
 sh symbolicate.sh <MyApp.crash> <MyApp.app.dSYM> <output file>
 **/

@interface SymbolicateTask()
@property (nonatomic, strong) NSTask *sybolicateTask;

@end

@implementation SymbolicateTask

- (void)performTaskForCompletionBlock:(TaskCompletionBlock)block
{
    NSString *sybolicateScriptPath;
    
    sybolicateScriptPath = [[NSBundle mainBundle] pathForResource:@"symbolicate" ofType:@"sh"];
    [super performTaskForCompletionBlock:block];
    
    self.sybolicateTask = [[NSTask alloc] init];
    [self.sybolicateTask setLaunchPath:sybolicateScriptPath];
    self.sybolicateTask.launchPath = @"/bin/bash";

    self.sybolicateTask.arguments = @[
                                      sybolicateScriptPath,
                                      self.crashLogPath,
                                      self.dSYMPath,
                                      self.outputPath
                                      ];

    [self.sybolicateTask setCurrentDirectoryPath:self.sandboxPath];
    
    NSPipe *pipe = [NSPipe pipe];
    [self.sybolicateTask setStandardInput:pipe];
    [self.sybolicateTask setStandardOutput:pipe];
    [self.sybolicateTask setStandardError:pipe];
    [self launchTask:self.sybolicateTask];
}

@end
