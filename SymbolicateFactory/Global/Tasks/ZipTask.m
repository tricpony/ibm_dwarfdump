//
//  ZipTask.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/23/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "ZipTask.h"

@interface ZipTask()
@property (nonatomic, strong) NSTask *zipTask;

@end

@implementation ZipTask

- (void)performTaskForCompletionBlock:(TaskCompletionBlock)block
{
    [super performTaskForCompletionBlock:block];
    
    self.zipTask = [[NSTask alloc] init];
    [self.zipTask setLaunchPath:@"/usr/bin/zip"];
    [self.zipTask setCurrentDirectoryPath:self.sandboxPath];
    [self.zipTask setArguments:@[@"-qry", self.destinationPath, @"."]];
    
    NSPipe *pipe = [NSPipe pipe];
    [self.zipTask setStandardInput:pipe];
    [self.zipTask setStandardOutput:pipe];
    [self.zipTask setStandardError:pipe];
    [self launchTask:self.zipTask];
}

@end
