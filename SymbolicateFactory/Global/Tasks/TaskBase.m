//
//  TaskBase.m
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/21/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "TaskBase.h"

@interface TaskBase()
@property (nonatomic, strong) TaskCompletionBlock completionBlock;

@end

@implementation TaskBase

- (void)performTaskForCompletionBlock:(TaskCompletionBlock)block
{
    self.completionBlock = block;
}

- (void)launchTask:(NSTask*)aTask
{

    if ([aTask isRunning]) {
        NSLog(@"How the hell is this happening?");
        return;
    }
    
    [aTask launch];
    [aTask waitUntilExit];
    self.completionBlock(nil,aTask);
    self.completionBlock = nil;
}

@end
