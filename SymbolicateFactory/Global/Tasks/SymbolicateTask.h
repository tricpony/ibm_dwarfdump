//
//  SymbolicateTask.h
//  SymbolicateFactory
//
//  Created by ANTHONY B ARTHUR on 10/23/15.
//  Copyright © 2015 IBM-Verse. All rights reserved.
//

#import "TaskBase.h"

@interface SymbolicateTask : TaskBase
@property (nonatomic, strong) NSString *crashLogPath;
@property (nonatomic, strong) NSString *dSYMPath;
@property (nonatomic, strong) NSString *outputPath;

@end
