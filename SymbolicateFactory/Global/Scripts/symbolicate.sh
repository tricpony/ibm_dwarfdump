#!/bin/sh

# Written by Kevin Kinney
# Email: kpkinn@us.ibm.com

echo "Symbolicating crash log..."

if [ "$DEVELOPER_DIR" == "" ] ; then
    export DEVELOPER_DIR="/Applications/Xcode.app/Contents/Developer"
fi

error_file="symbolicate_error"
output_file="symbolicated.crash"
if [ $# -gt 2 ] ; then
	output_file=$3;
fi
echo "Using $output_file as output file"

if [ $# -gt 1 ] ; then
    # check the paramaters
    crash_file=$1
    dsym_file=$2

    if ! [ -e "$crash_file" ] ; then
        echo "ERROR: Can't find crash file"
        exit 1
    fi

    if ! [ -e "$dsym_file" ] ; then
        echo "ERROR: Can't find .dSYM file"
        exit 1
    fi

    if [ ${crash_file#*.} != "crash" ] ; then
        echo "WARNING: The first argument should be the crash file but does not have the \".crash\" extension"
    fi

    if [ ${dsym_file#*.} != "app.dSYM" ] ; then
            echo "WARNING: The second argument should be the dSYM file but does not have the \".dSYM\" extension"
    fi

    # Do the actual symbolication.
    # Note: Xcode 4 path was: /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/PrivateFrameworks/DTDeviceKitBase.framework/Versions/A/Resources/symbolicatecrash
    # Xcode 5 path: /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/PrivateFrameworks/DTDeviceKitBase.framework/Versions/A/Resources/symbolicatecrash -v $crash_file $app_file > $output_file 2> $error_file

    /Applications/Xcode.app/Contents/SharedFrameworks/DTDeviceKitBase.framework/Versions/A/Resources/symbolicatecrash -v $crash_file $dsym_file > $output_file 2> $error_file
	rc=$?
	if [ $rc != 0 ] ; then
		echo "ERROR: Symbolication failed with return code $rc. Error output can be found in $error_file"
	else
		echo "Symbolication successful! Output is in $output_file"
	fi
else 
	echo "ERROR: Not enough parameters. Use the formate \"symbolicate.sh <MyApp.crash> <MyApp.app.dSYM> <output file>\""
fi
