//
//  ConstantsDefines.h
//
//  Created by ANTHONY B ARTHUR on 10/14/15.
//  Copyright © 2015 IBM Corp. 2015. All rights reserved.

#import "AppDelegate.h"

#pragma mark -
#pragma mark Type Definitions

#define APP_DELEGATE ((AppDelegate*)[NSApplication sharedApplication].delegate)

typedef enum
{
    kCodeSignType = 0,
    kCodeBuildType,
    kUnknownEntryType
}  ALDTransactionType;
